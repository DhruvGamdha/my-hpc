
double CompSimpson(double a, double b, int n, double (*f)(double))
{
    // Simpson's rule for integration using OpemMP
    // Simpson's rule is a composite rule that approximates the integral of a function f(x)
    // from a to b using n intervals.

    #ifdef _OPENMP
        const int my_rank = omp_get_thread_num();
        const int thread_count = omp_get_num_threads();
    #else
        const int my_rank = 0;
        const int thread_count = 1;
    #endif

    double h = (b - a) / ((double)n);

    double local_N = (double)n / (double)thread_count;
    double local_a = a + my_rank * local_N * h;
    double local_b = local_a + local_N * h;
    double local_x = 0;
    double local_sum = f(local_a) + f(local_b);

    for (int i = 1; i < local_N/2; i++)
    {
        local_x = local_a + 2*i * h;
        local_sum += 2 * f(local_x);
        local_sum += 4 * f(local_x - h);
    }

    local_sum += 4 * f(local_a + (local_N-1) * h);
    local_sum *= h / 3;

    return local_sum;
}