#include <stdio.h>
#include <time.h>
// Include RAND_MAX from stdlib.h
#include <stdlib.h>
// include openMP header file
#ifdef _OPENMP
    #include <omp.h>
#endif
#include "simpson.h"
#include <math.h>

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], int *N, int *num_threads)
{
    if (argc != 3 && argc != 2)
    {
        printf("Usage: ./main <num_threads> <N> or ./main <N>\n");
        exit(1);
    }
    if (argc == 3)
    {
        *num_threads = atoi(argv[1]);
        *N = atoi(argv[2]);
        if (*N <= 0 || *N%2 != 0)
        {
            printf("N must be a positive even integer\n");
            exit(1);
        }
        if (*num_threads <= 0)
        {
            printf("num_threads must be a positive integer\n");
            exit(1);
        }
    }
    else
    {
        *num_threads = 1;
        *N = atoi(argv[1]);
        if (*N <= 0 || *N%2 != 0)
        {
            printf("N must be a positive even integer\n");
            exit(1);
        }
    }
}

// Function whose integral is to be approximated
double f(double x)
{
    // exponential function
    return exp(-8*x*x);
}


// Main function
int main(int argc, char *argv[])
{
    // Get user input
    int N, num_threads;
    get_input(argc, argv, &N, &num_threads);

    double integral = 0;
    double exact_integral = 0.62661737464261433833;
    // Get start time
    double start = omp_get_wtime();
    #pragma omp parallel num_threads(num_threads)
    {
        // Compute integral using Simpson's rule
        double integral_local = CompSimpson(-1, 1, N, f);
        #pragma omp critical
        integral += integral_local;
    }

    // Get end time
    double end = omp_get_wtime();

    // Print results
    printf("Integral = %.16f\n", integral);
    // Print error
    printf("Error = %.16f\n", fabs(integral - exact_integral));
    printf("Time taken = %.16f seconds\n", (double)(end - start));
    return 0;
}
