from cProfile import label


def getdata(infile):
    '''
    INPUT: 
    -----
    infile is a string and refers to a file 
    that has 2 columns, the first column has 
    the x-coordinates and the second column
    has the y-coordinates of the input data.
    
    WHAT THIS FUNCTION DOES:
    ------------------------

    This function reads in all the data from the
    file "infile" and returns the data as 2 vectors
    (numpy arrays): x and y.


    OUTPUT:
    -------
    2 numpy arrays (vectors): x and y
    '''
    import numpy as np

    file = open(infile, "r")
    x = []
    y = []
    while True:
        line = file.readline()
        if line:
            vals = line.split()
            x.append(float(vals[0]))
            y.append(float(vals[1]))
        else:
            break
    x = np.array(x)
    y = np.array(y)
    return x, y


def approx(x, y):
    '''
    INPUT:
    ------
    x and y are 2 numpy arrays (vectors) of
    the same length.
    WHAT THIS FUNCTION DOES:
    ------------------------
    This function takes the data (x,y) and calculates
    the 3 coefficients, a[0], a[1], and a[2], of a quadratic function:
            y = a[0] + a[1]*x + a[2]*x**2
    The coefficients are computed by forcing the quadratic
    polynomial to give the best-fit to the input data in
    the least-squares sense.
    OUTPUT:
    -------
    numpy array (vector) of length 3: a.
    '''
    import numpy as np

    A = np.ones((len(x), 3))
    b = y
    
    A[:, 1] = x
    A[:, 2] = np.power(x, 2)

    AT = np.transpose(A)
    a = np.linalg.solve(np.matmul(AT, A), np.matmul(AT, b))
    return a

def plotdata(infile):
    '''
    INPUT:
    ------
    x and y are 2 numpy arrays (vectors) of
    the same length. a is a numpy array (vector)
    of length 3. infile is a string that is the
    name of the input data file.
    
    WHAT THIS FUNCTION DOES:
    ------------------------
    This function takes the data (x,y) and
    the 3 coefficients, a[0], a[1], and a[2], of a quadratic function:
            y = a[0] + a[1]*x + a[2]*x**2
    and plots them on the same graph.
    
    OUTPUT:
    -------
    None. A PNG image file is created in the current directory.
    '''
    import numpy as np
    import matplotlib.pyplot as plt

    x, y = getdata(infile)
    a = approx(x, y)

    numPoints = 1000
    minX = min(x)
    maxX = max(x)

    testX = np.linspace(minX, maxX, numPoints, endpoint=True)
    matX = np.ones((numPoints, 3))
    matX[:,1] = testX
    matX[:,2] = np.power(testX, 2)

    testY = np.matmul(matX, a)
    plt.plot(x, y, label="Input Data Points")
    plt.plot(testX,testY, label="Quadratic Interpolation Plot")
    plt.title(infile + ": Data + Least Square Fit w/ Quad. Poly.")
    plt.legend(loc="best")
    plt.savefig(infile + '.png', bbox_inches='tight')
    plt.close()

if __name__ == "__main__":
    plotdata('xydata1.dat')
    plotdata('xydata2.dat')
    plotdata('xydata3.dat')
    plotdata('xydata4.dat')
