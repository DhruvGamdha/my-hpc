#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void Swap2Nodes(node** head, const int pos1, const int pos2)
{
    node* ptr1 = NULL;  // pointer for pos1
    node* ptr2 = NULL;  // pointer for pos2
    node* temp = NULL;  // temporary pointer
    
    // allocate memory for temp
    temp = (node*)malloc(sizeof(node));

    // find pos1 and pos2
    for (node* ptr = *head; ptr != NULL; ptr = ptr->next)
    {
        if (ptr->position == pos1)
        {
            ptr1 = ptr;
        }
        if (ptr->position == pos2)
        {
            ptr2 = ptr;
        }
    }

    // check if pos1 and pos2 are valid
    if (ptr1 == NULL || ptr2 == NULL)
    {
        printf("\n Error: position not found.\n");
        printf("  pos1 = %i\n",pos1);
        printf("  pos2 = %i\n",pos2);
        exit(1);
    }

    // Swap the values
    temp->value = ptr1->value;
    ptr1->value = ptr2->value;
    ptr2->value = temp->value;

    // free the temporary pointer
    free(temp);
}