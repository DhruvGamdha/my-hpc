#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void SortList(node** head)
{
   // quick sort the list.
   
   // Find the list length.
   int length = GetListLength(*head);

   // If the list is empty or has only one node, return.
   if (length <= 1)
   {
      return;
   }

   // Sort the list.
   quickSort(head,1,length);
   
}

int GetListLength(const node* head)
{
   int length = 0;
   node* ptr = head;
   while(ptr!=NULL)
   {
      length = length + 1;
      ptr = ptr->next;
   }
   return length;
}

void quickSort(node** head,const int start,const int end)
{
   // Sort the list.
   if(start<end)
   {
      int pivot = partition(head,start,end);
      quickSort(head,start,pivot-1);
      quickSort(head,pivot+1,end);
   }
   ArrangeIndex(head);
}

int partition(node** head,const int start,const int end)
{
   // Partition the list.
   int pivot = start;
   node* ptr = *head;
   node* ptr2 = *head;
   // node* temp = NULL;
   int temp_value;
   // int temp_position;
   // int pos = start;
   while(ptr!=NULL)
   {
      ptr2 = ptr;
      while(ptr2->next!=NULL)
      {
         if(ptr2->value > ptr2->next->value)
         {
            temp_value = ptr2->value;
            // temp_position = ptr2->position;
            ptr2->value = ptr2->next->value;
            // ptr2->position = ptr2->next->position;
            ptr2->next->value = temp_value;
            // ptr2->next->position = temp_position;
         }
         ptr2 = ptr2->next;
      }
      ptr = ptr->next;
   }
   // ptr = *head;
   // while(ptr!=NULL)
   // {
   //    ptr->position = pos;
   //    pos = pos+1;
   //    ptr = ptr->next;
   // }
   return pivot;
}

void ArrangeIndex(node** head)
{
   // Arrange the index of the list.
   node* ptr = *head;
   int pos = 1;
   while(ptr!=NULL)
   {
      ptr->position = pos;
      pos = pos+1;
      ptr = ptr->next;
   }
}