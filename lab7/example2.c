#include <stdio.h>


int main()
{
    int N = 0;
    printf("Input odd integer N:");
    scanf("%d", &N);
    // printf("Your selection is %d\n",N);
    int count = 1;

    // Check whether N is negative.
    if (N < 0)
    {    
        printf("Input given is negative.");
        return 0;
    }

    // Check whether N is odd
    if (N % 2 == 0)
    {
        printf("Input given is not odd.");
        return 0;
    }
    
    for(int i =1; i <= N; i = i + 2)
    {
        printf(" %d ", i);
        count++;
        if(count > 5)
        {
            printf("\n");
            count = 1;
        }
    }
    return 0;
}