#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void Verlet(const int whichrun,
            const double u0,
            const double v0,
            const double dt,
            const int NumSteps)
{
    double u[NumSteps + 1];
    double v[NumSteps + 1];

    // Initial Conditions
    u[0] = u0;
    v[0] = v0;

    // Main Loop
    double vStar = 0;
    
    for(int n = 0; n < NumSteps; n++)
    {
        vStar = v[n] + (dt/2.0)*(u[n] - pow(u[n], 3));
        u[n+1] = u[n] + dt * vStar;
        v[n+1] = vStar + (dt/2.0)*(u[n+1] - pow(u[n+1], 3));
    }

    // Print solution to file
    char filename[] = "outputX.data";
    filename[6] = whichrun + '0';
    FILE* outfile = fopen(filename, "w");
    // std::string u_writter;
    // std::string v_writter;
    for(int i=0; i<=NumSteps; i++)
    {
        fprintf(outfile, "%lf %lf \n", u[i], v[i]);
    }

    fclose(outfile);
}

int main()
{
    void Verlet(const int whichrun,
                const double u0,
                const double v0,
                const double deltat,
                const int NumSteps);

    const double Tfinal = 12.0;
    const int NumSteps = 1000;
    const double dt = Tfinal/NumSteps;

    Verlet(1, 1.0, 0.4, dt, NumSteps);
    Verlet(2, 1.0, 0.6, dt, NumSteps);
    Verlet(3, 1.0, 0.8, dt, NumSteps);
    Verlet(4, -1.0, 0.6, dt, NumSteps);
    Verlet(5, -1.0, 0.4, dt, NumSteps);
}