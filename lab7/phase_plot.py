from tokenize import Double
import numpy as np
import matplotlib.pyplot as plt
from pip import main


def readingFunc(filename):
    file = open(filename, "r")
    u = []
    v = []
    while True:
        line = file.readline()
        if line:
            vals = line.split()
            u.append(np.double(vals[0]))
            v.append(np.double(vals[1]))
        else:
            break
    u = np.array(u)
    v = np.array(v)
    return u, v

def phase_plot():
    u1, v1 = readingFunc("output1.data")
    u2, v2 = readingFunc("output2.data")
    u3, v3 = readingFunc("output3.data")
    u4, v4 = readingFunc("output4.data")
    u5, v5 = readingFunc("output5.data")
    # u = u3
    # v = v3
    # print(len(u), u)
    # print(len(v), v)
    # plt.figure()
    plt.plot(u1, v1, label="output1")
    plt.plot(u2, v2, label="output2")
    plt.plot(u3, v3, label="output3")
    plt.plot(u4, v4, label="output4")
    plt.plot(u5, v5, label="output5")

    plt.title("u-v plot of 5 files")
    # plt.axis([-2, 2, -2, 2])
    # plt.xticks(np.arange(-3, 3, 0.10))
    # plt.yticks(np.arange(-3, 3, 0.10))
    plt.legend(loc="best")
    plt.savefig('plot.png', bbox_inches='tight')
    # plt.show()
    plt.close()

if __name__ == "__main__":
    phase_plot()
