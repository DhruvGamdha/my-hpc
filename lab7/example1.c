#include <stdio.h>


int main()
{
    int N = 53;
    int count = 1;

    // Check whether N is odd
    if (N % 2 == 0)
        N = N - 1;
    // printf("N : %d \n", N);
    for(int i =1; i <= N; i = i + 2)
    {
        printf(" %d ", i);
        count++;
        if(count > 5)
        {
            printf("\n");
            count = 1;
        }
    }
    return 0;
}