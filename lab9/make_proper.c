#include <stdlib.h>
#include "fraction.h"
#include "properfraction.h"

void make_proper(const fraction* in,
                  properfraction* out)
{
   fraction temp;
   
   // Shifting the negative value in the denominator to the numerator.
   if (in->denom < 0)
   {
      temp.numer = -1 * in->numer;
      temp.denom = -1 * in->denom;
   }
   else
   {
      temp.numer = in->numer;
      temp.denom = in->denom;
   }

   // Case with zero whole part
   if (abs(temp.numer) < abs(temp.denom))
   {
      out->whole = 0;
      out->numer = temp.numer;
      out->denom = temp.denom;
   }
   else
   {
      int count = 0;
      int val = 1;
      while(1)
      {
         if (abs(val*temp.denom) < abs(temp.numer))
         {
            val = val * temp.denom;
            count++;
         }
         else
            break;
      }

      if (temp.numer < 0)
      {
         out->whole = -1*count;
         out->denom = temp.denom;
         out->numer = -1*temp.numer - val;
      }
      else
      {
         out->whole = count;
         out->denom = temp.denom;
         out->numer = temp.numer - val;
      }
   }

}