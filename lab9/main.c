#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fraction.h"
#include "properfraction.h"

int main()
{
   fraction a,b,sum, diff, prod, quot;
   properfraction propSum, propDiff, propProd, propQuot;

   printf("Input numerator of a:");
   scanf("%d", &a.numer);

   printf("Input denominator of a:");
   scanf("%d", &a.denom);

   printf("Input numerator of b:");
   scanf("%d", &b.numer);

   printf("Input denominator of b:");
   scanf("%d", &b.denom);

   void fraction_add(const fraction* a,
                     const fraction* b,
                     fraction* sum);

   void fraction_subtract( const fraction* a, 
                           const fraction* b,
                           fraction* diff);
   
   void fraction_mult(  const fraction* a, 
                        const fraction* b,
                        fraction* prod);

   void fraction_divide(const fraction* a, 
                        const fraction* b,
                        fraction* quot);

   void make_proper(const fraction* in,
                  properfraction* out);
   
   void printPropFraction(properfraction val);

   fraction_add(&a,&b,&sum);
   fraction_subtract(&a, &b, &diff);
   fraction_mult(&a, &b, &prod);
   fraction_divide(&a, &b, &quot);

   make_proper(&sum, &propSum);
   make_proper(&diff, &propDiff);
   make_proper(&prod, &propProd);
   make_proper(&quot, &propQuot);

   printf("\n %i/%i  +  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propSum);

   printf("\n %i/%i  -  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propDiff);

   printf("\n %i/%i  x  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propProd);

   printf("\n %i/%i  /  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propQuot);

   return 0;

}

void printPropFraction(properfraction val)
{
   if (abs(val.whole)>0)
     { printf("%i(%i/%i)",val.whole,val.numer,val.denom); }
   else
     { printf("%i/%i",val.numer,val.denom); }
   printf("\n");
}