#include <stdio.h>
#include <time.h>
// Include RAND_MAX from stdlib.h
#include <stdlib.h>
#include <math.h>

// Add MPI header file
#include <mpi.h>

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], const int my_rank, const int comm_sz, int *N)
{
    void usage(const char* prog_name);

    if (my_rank == 0)
    {
        if (argc != 2){usage(argv[0]);}
        *N = atoi(argv[1]);
        if (*N <= 0){usage(argv[0]);}
        if (*N % comm_sz != 0){usage(argv[0]);}

        for (int i = 0; i < comm_sz; i++)
            MPI_Send(N, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
    }
    else
        MPI_Recv(N, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

void usage(const char* prog_name)
{
    fprintf(stderr, "Usage: %s <N>\n", prog_name);
    fprintf(stderr, "   N should be positive\n");
    fprintf(stderr, "   N should be exactly divisible by the number of processors\n");
    exit(1);
}

int PiEstimation(int N)
{
    // Randomly generate x and y coordinates between 0 and 1
    double x, y;
    int i;
    int inside = 0;

    for (i = 0; i < N; i++)
    {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        if (x*x + y*y <= 1)
            inside++;
    }
    
    return inside;
}

// Main function
int main(int argc, char *argv[])
{
    // Setup MPI    
    int comm_sz, my_rank;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // Get user input
    int N;
    void get_input(int argc, char *argv[], const int my_rank, const int comm_sz, int *N);
    get_input(argc, argv, my_rank, comm_sz, &N);

    // Exact value of Pi
    double pi_exact = 3.14159265358979323846;
    int inside_local = 0;
    int inside_global = 0;
    const int N_local = N/comm_sz;

    // Get start time
    double start;
    if (my_rank == 0)
        start = MPI_Wtime();
    
    // Estimate Pi
    inside_local += PiEstimation(N_local);

    if (my_rank !=0)
        MPI_Send(&inside_local, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    else
    {
        inside_global = inside_local;
        for (int i = 1; i < comm_sz; i++)
        {
            MPI_Recv(&inside_local, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            inside_global += inside_local;
        }
    }

    // Get end time and print results
    double end;
    if (my_rank == 0)
    {
        end = MPI_Wtime();
        printf("\n\nPi estimate: %f\n", 4.0 * inside_global / N);
        printf("Pi exact: %f\n", pi_exact);
        printf("Time: %f\n", end - start);
        printf("Error: %f\n", fabs(pi_exact - 4.0 * inside_global / N));
        printf("\n\n");
    }   

    // Finalize MPI
    MPI_Finalize();
    return 0;
}
