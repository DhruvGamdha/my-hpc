#include <stdio.h>
#include <time.h>
// Include RAND_MAX from stdlib.h
#include <stdlib.h>
#include <math.h>

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], int *N)
{
    if (argc != 2)
    {
        printf("Usage: ./main <N>\n");
        exit(1);
    }

    *N = atoi(argv[1]);
    if (*N <= 0)
    {
        printf("N must be a positive integer\n");
        exit(1);
    }
}

double PiEstimation(int N)
{
    // Randomly generate x and y coordinates between 0 and 1
    double x, y;
    int i;
    int inside = 0;
    double pi;
    double start_time, end_time;

    for (i = 0; i < N; i++)
    {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        if (x*x + y*y <= 1)
        {
            inside++;
        }
    }
    pi = 4.0 * inside / N;
    return pi;
}

// Main function
int main(int argc, char *argv[])
{
    // Get user input
    int N;
    get_input(argc, argv, &N);

    // Exact value of Pi
    double pi_exact = 3.14159265358979323846;

    // Get start time
    double start = clock();
    
    // Estimate Pi
    double pi = PiEstimation(N);

    // Get end time
    double end = clock();

    // Print N, time taken, and error in single line
    printf("%d %f %f\n", N, (end - start) / CLOCKS_PER_SEC, fabs(pi - pi_exact));

    // // Print results
    // printf("Estimated value of Pi: %f\n", pi);

    // // Print time taken
    // printf("CPU Time taken = %.16f seconds\n", (double)(end - start)/CLOCKS_PER_SEC);
    
    // // Print error
    // printf("Error = %.16f\n", fabs(pi - pi_exact));

    return 0;
}
