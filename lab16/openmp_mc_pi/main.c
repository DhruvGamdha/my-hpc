#include <stdio.h>
#include <time.h>
// Include RAND_MAX from stdlib.h
#include <stdlib.h>
#include <math.h>

// Add OpenMP header file
#ifdef _OPENMP
    #include <omp.h>
#endif

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], int *N, int *num_threads)
{
    if (argc != 3)
    {
        printf("Usage: ./main <num_threads> <N>\n");
        exit(1);
    }

    *num_threads = atoi(argv[1]);
    *N = atoi(argv[2]);
    if (*N <= 0)
    {
        printf("N must be a positive integer\n");
        exit(1);
    }
    if (*num_threads <= 0)
    {
        printf("num_threads must be a positive integer\n");
        exit(1);
    }
}

int PiEstimation(int N)
{   

    #ifdef _OPENMP
        const int thread_count = omp_get_num_threads();
    #else
        const int thread_count = 1; 
    #endif

    int local_N = N / thread_count;

    // Randomly generate x and y coordinates between 0 and 1
    double x, y;
    int i;
    int inside = 0;
    
    for (i = 0; i < local_N; i++)
    {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        if (x*x + y*y <= 1)
        {
            inside++;
        }
    }
    return inside;
}

// Main function
int main(int argc, char *argv[])
{
    // Get user input
    int N, num_threads;
    get_input(argc, argv, &N, &num_threads);

    // Exact value of Pi
    double pi_exact = 3.14159265358979323846;
    int inside_global = 0;

    // Get start time
    double start = omp_get_wtime();
    
    // Estimate Pi
    #pragma omp parallel num_threads(num_threads) reduction(+:inside_global)
    inside_global += PiEstimation(N);

    double pi = 4.0 * inside_global / N;
    // Get end time
    double end = omp_get_wtime();

    // Print N, time taken, and error in single line
    printf("%d %f %f\n", N, (end - start), fabs(pi - pi_exact));

    return 0;
}
