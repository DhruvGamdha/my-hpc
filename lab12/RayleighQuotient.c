#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "matrix.h"

matrix createIdentity(int size)
{
  matrix mat = new_matrix(size,size);
  for (int i=1; i<=size; i++)
    for (int j=1; j<=size; j++)
      {
        if (i==j) {mget(mat,i,j) = 1.0;}
        else {mget(mat,i,j) = 0.0;}
      }
  return mat;
}

matrix matrix_scalar_mult(const matrix* A, const double scalar)
{
  const int rows = A->rows;
  const int cols = A->cols;
  matrix C = new_matrix(rows,cols);
  for (int i=1; i<=rows; i++)
    for (int j=1; j<=cols; j++)
      {
        mget(C,i,j) = mgetp(A,i,j)*scalar;
      }
  return C;
}

double RQiter(vector* v, double TOL, int MaxIters, matrix* A)
{
    const int size = v->size;
    assert(size>0);
    assert(size==A->rows); assert(size==A->cols);

    double lambda = 1.0e0;
    double lambda_old = 0.0e0;

    vector v_new = new_vector(size);
    vector w = new_vector(size);

    // Creating identity matrix
    matrix Iden = createIdentity(size);
    
    matrix scaledIdent = new_matrix(size,size); 

    // Create matrix temp
    matrix temp = new_matrix(size,size);

    double mag_v = vector_mag(v);
    double mag_w = 1;

    // Set v_new to v with magnitude 1
    for(int i = 1; i<=size; i++)
        { vget(v_new,i) = vgetp(v,i)/mag_v; }

    // Evaluate lambda
    vector Ax = matrix_vector_mult(A, &v_new);
    lambda = vector_dot_mult(&v_new, &Ax);

    // Iterate
    int k = 0;
    int mstop = 0;
    while (k<MaxIters && mstop==0)
    {
        k++;
        scaledIdent = matrix_scalar_mult(&Iden, lambda);
        temp = matrix_sub(A, &scaledIdent);
        w = solve(&temp, &v_new);
        mag_w = vector_mag(&w);

        // Set v to w with magnitude 1
        for(int i = 1; i<=size; i++)
        { vget(v_new,i) = vget(w,i)/mag_w; }

        // Evaluate lambda
        Ax = matrix_vector_mult(A, &v_new);
        lambda_old = lambda;
        lambda = vector_dot_mult(&v_new, &Ax);

        // Check for convergence
        if (fabs(lambda-lambda_old)<TOL)
        {
        mstop = 1;
        printf("\n Eigenvalue converged in %d iterations\n", k);
        }

    }
    return lambda;
}


vector Inv_power_iteration(vector* v, double TOL, int MaxIters, matrix* A)
{
    const int size = v->size;
    assert(size>0);
    assert(size==A->rows); assert(size==A->cols);

    vector v_new = new_vector(size);
    vector w = new_vector(size);

    double mag_v = vector_mag(v);
    double mag_w = 1;

    // Set v_new to v with magnitude 1
    for(int i = 1; i<=size; i++)
        { vget(v_new,i) = vgetp(v,i)/mag_v; }

    // Iterate
    int k = 0;

    while (k<10 )
    {
        k++;
        w = solve(A, &v_new);
        mag_w = vector_mag(&w);

        // Set v to w with magnitude 1
        for(int i = 1; i<=size; i++)
        { vget(v_new,i) = vget(w,i)/mag_w; }

    }
    return v_new;
}

