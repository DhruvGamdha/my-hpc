#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

int main()
{
  // New Matrix and Vector
  printf("Input N\n");
  int N;
  scanf("%d", &N);
  matrix A = generateTridiagonal(N, -1, 2, -1);
  // matrix A = new_matrix(3,3);
  vector v = new_vector(N);

  // // Set values
  // mget(A,1,1) = 2.0;
  // mget(A,1,2) = 1.0;
  // mget(A,1,3) = 1.0;
  // mget(A,2,1) = 1.0;
  // mget(A,2,2) = 3.0;
  // mget(A,2,3) = 1.0;
  // mget(A,3,1) = 1.0;
  // mget(A,3,2) = 1.0;
  // mget(A,3,3) = 4.0;

  // vget(v,1) = 1.0;
  // vget(v,2) = 1.0;
  // vget(v,3) = 1.0;
  
  // Initialize v
  for (int i=1; i<=N; i++)
  {
    vget(v,i) = 1.0;
  }

  // Print Matrix and Vector
  print_matrix(&A);
  print_vector(&v);

  // Find largest eigenvalue
  v = Inv_power_iteration(&v, 1e-8,500, &A);
  double lambda = RQiter(&v,1e-8,500, &A);
  printf("\nLargest Eigenvalue: %.10f\n", lambda);

  // Cleanup
  delete_matrix(&A);
  delete_vector(&v);
}
