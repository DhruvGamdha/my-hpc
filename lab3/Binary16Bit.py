def bin2int(bin_string):

    if isinstance(bin_string, str) != True:
        return "Input not a string."

    if len(bin_string) != 16:
        return "Input character length is not 16."

    a = []
    intVal = 0

    # Reading and storing the binary string in reverse order.
    for i in range(len(bin_string)):
        val = bin_string[len(bin_string) -1 - i]
        if val == '0' or val == '1':
            a.append(int(val))
        else:
            return "Input string do not contain all values as 1 or 0."

    # Computing the value in base 10
    intVal += -(2**(len(bin_string)-1))*a[len(bin_string)-1]
    for i in range(len(bin_string)-1):
        intVal += (2**i)*a[i]

    return intVal


def int2bin(x):

    if isinstance(x, int) != True:
        return "Input not a integer."

    if x < -32768 or x > 32767:
        return "Number too large or too small."

    bin_string = [];

    if x==0:
        for n in range(0,16):
            bin_string.append('0')
    else:
        if x<0:
            tmp = '1'
            xmod = x + 2**15
        else:
            tmp = '0'
            xmod = x
        
        bin_string.append(tmp)

        for n in range(1, 16):
            pw = 2**(15-n)
            z = int(xmod/pw)
            xmod = xmod - z*pw
            tmp = str(z)
            bin_string.append(tmp)

    bin_string = ''.join(bin_string)

    return bin_string
    
def test():
    ## bin2int testing
    print(bin2int('0000000000000000'))  ## Zero
    print(bin2int('1111111100000010'))  ## Negative 1
    print(bin2int('1111111100110010'))  ## Negative 2
    print(bin2int('0111111100110010'))  ## Positive 1
    print(bin2int('0111111100100010'))  ## Positive 2

    ## int2bin testing (reverse of bin2int testing)
    print(int2bin(0))
    print(int2bin(-254))
    print(int2bin(-206))
    print(int2bin(32562))
    print(int2bin(32546))  

def add2bins(bin1, bin2):
    
    if isinstance(bin1, str) == False or isinstance(bin2, str) == False:
        return "Input not a string."

    if len(bin1) != 16 or len(bin2) != 16:
        return "Input character length is not 16."

    str1 = []
    str2 = []
    addn = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    # Reading and storing the binary string in reverse order.
    for i in range(len(bin1)):
        val1 = bin1[len(bin1) -1 - i]
        val2 = bin2[len(bin2) -1 - i]
        if val1 == '0' or val1 == '1':
            str1.append(int(val1))
        else:
            return "Input string do not contain all values as 1 or 0."
        
        if val2 == '0' or val2 == '1':
            str2.append(int(val2))
        else:
            return "Input string do not contain all values as 1 or 0."

    ## Addition of str1 and str2 element
    for i in range(len(bin1)):
        temp = str1[i] + str2[i] + addn[i]
        if temp == 2:
            addn[i]     = 0
            addn[i+1]   = 1
        elif temp == 3:
            addn[i]     = 1
            addn[i+1]   = 1
        else:
            addn[i] = temp
    
    bin_out = ''
    addnLen = len(addn)
    for i in range(addnLen):
        bin_out = bin_out + str(addn[addnLen -1 - i])

    return bin_out


# print(add2bins('0000000000000011', '0000000000000011'))
# test()

# print(bin2int('0000000000000001'))
# print(bin2int('0000000000000010'))
# print(int2bin(-254))
# print(int2bin(32768))