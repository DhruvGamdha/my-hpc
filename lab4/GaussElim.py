def LUfactor(A):
    from numpy import array
    from numpy import shape
    from numpy import identity
    # print(shape(A)[0])
    U = np.copy(A)
    n = shape(A)[0]
    L = identity(n)
    # print(shape(U)[0])
    for k in range(n-1):
        for j in range(k+1, n):
            L[j, k] = U[j, k] / U[k, k]
            for i in range(k, n):
                U[j, i] = U[j, i] - L[j, k]*U[k, i]

    return L, U

def determinent(A):
    L, U = LUfactor(A)
    n = np.shape(A)[0]
    det = 1
    for i in range(n):
        det = det * U[i,i]

    return det

def testing(A):
    import numpy as np
    L, U = LUfactor(A)
    print("L:")
    print(L)

    print("U:")
    print(U)

    print("A:")
    print(A)

    print("LU:")
    A_bar = np.dot(L,U)
    print(A_bar)

    # print("det A    :", np.linalg.det(A))
    print("det func :", determinent(A))

# A1 = np.matrix([[5, -1, 2], [10, 3, 7], [15, 17, 19]])
# A2 = np.matrix([[4, 1, 0, 0], [1, 4, 1, 0], [0, 1, 4, 1], [0, 0, 1, 4]], dtype=float)
# A3 = np.matrix([[1, -2, -2, -3], [3, -9, 0, -9], [-1, 2, 4, 7], [-3, -6, 26, 2]])
# testing(A2) 