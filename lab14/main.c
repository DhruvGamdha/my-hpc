#include "matrix.h"
#include <stdio.h>
#include <time.h>
// Include RAND_MAX from stdlib.h
#include <stdlib.h>
// include openMP header file
#ifdef _OPENMP
    #include <omp.h>
#endif

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], int *N, int *num_threads)
{
    if (argc != 3)
    {
        printf("Usage: ./main <N> <num_threads>\n");
        exit(1);
    }
    *N = atoi(argv[1]);
    *num_threads = atoi(argv[2]);
    if (*N <= 0)
    {
        printf("N must be a positive integer\n");
        exit(1);
    }
    if (*num_threads <= 0)
    {
        printf("num_threads must be a positive integer\n");
        exit(1);
    }
}

// main function
int main(int argc, char *argv[])
{
    // Create MATRIX A and Vector x
    int N;
    int num_threads;
    get_input(argc, argv, &N, &num_threads);
    
    matrix A = new_matrix(N, N);
    vector x = new_vector(N);

    // Time before initializing A and x
    double start_time = omp_get_wtime();

    // Initialize A and x with OpenMP
    #pragma omp parallel for num_threads(num_threads)
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            mget(A, i, j) = (double)rand() / RAND_MAX;
        }
        vget(x, i) = (double)rand() / RAND_MAX;
    }

    // Time after initializing A and x
    double end_time = omp_get_wtime();

    // Time before computing Ax
    double start_time_Ax = omp_get_wtime();

    // Matrix A and Vector x Multiplication loop using OpenMP
    #pragma omp parallel for num_threads(num_threads)
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            mget(A, i, j) = mget(A, i, j) * vget(x, j);
        }
    }

    // Time after computing Ax
    double end_time_Ax = omp_get_wtime();

    // Print out the time taken for each step
    printf("Time taken to initialize A and x: %f\n", end_time - start_time);
    printf("Time taken to compute Ax: %f\n", end_time_Ax - start_time_Ax);

    // Delete A and x
    delete_matrix(&A);
    delete_vector(&x);
}
