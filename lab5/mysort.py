def quicksort(Ain, lo=0, hi=-100):
    import numpy as np
    if lo < hi:
        p = partition(Ain, lo, hi)
        quicksort(Ain, lo, p-1)
        quicksort(Ain, p+1, hi)
    return Ain
    

def partition(A, lo, hi):
    pivot = A[hi]
    i = lo # Place for swapping
    for j in range(lo, hi):
        if A[j] <= pivot:
            # Swap A[i] to A[j]
            temp1 = A[i]
            A[i] = A[j]
            A[j] = temp1
            i = i + 1

    # Swap A[i] to A[hi]
    temp2   = A[i]
    A[i]    = A[hi]
    A[hi]   = temp2
    return i

def filesort():
    from read_input_file import read_input_file
    import numpy as np

    A = read_input_file()
    siz_A = len(A)

    print("len of arr   :", siz_A)
    A_sorted = np.copy(A)
    A_sorted = quicksort(A_sorted, 0, siz_A-1)
    # print("A sort   :", A_sorted)

    fid = open('sorted.data', 'w')    
    for k in range(0,siz_A):
        # value = rand.uniform(0.0, 1000.0)
        fid.write("%12.6e" % A_sorted[k])
        fid.write("\n");
    fid.close()

if __name__ == "__main__":
    filesort()