def read_input_file():
    '''
    This function reads the file "input.data"
    and stores the results in the NumPy array A.
    '''

    # First, figure out how many floats there are
    fid = open('input.data', 'r')
    kmax = 0;
    while True:
        line = fid.readline()
        if not line: break
        kmax = kmax+1
    fid.close()

    import numpy as np

    # Second, read-in all the floats
    A = np.zeros(kmax,dtype=float)
    fid = open('input.data', 'r')
    for k in range(0,kmax):
        linestring = fid.readline()
        linelist   = linestring.split()
        A[k]       = np.float64(linelist[0])
    fid.close()

    # Third, return the result
    return A;

if __name__ == "__main__":
    import numpy as np
    from mysort import quicksort

    # A = read_input_file()
    A = np.array([1.0, -2.0, 3.0, -4.0, 5.0, -6.0, 7.0], dtype=float)
    
    print ("Number of elements read = ",A.shape[0])
    print("A in :", A)
    A_sorted = np.copy(A)
    A_sorted = quicksort(A_sorted, 0, A_sorted.shape[0]-1)
    print("A sort   :", A_sorted)

