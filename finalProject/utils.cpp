#include "util.h"
#include "v3.h"
#include "tri.h"
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

void crossProduct(v3& a, v3& b, v3 *ans) 
{   
    ans->m_x = a.m_y*b.m_z - a.m_z*b.m_y;
    ans->m_y = a.m_z*b.m_x - a.m_x*b.m_z;
    ans->m_z = a.m_x*b.m_y - a.m_y*b.m_x;
}

double dotProduct(v3& a, v3& b)
{   
    return a.m_x*b.m_x + a.m_y*b.m_y + a.m_z*b.m_z;
}

void addValue(double *ts_arr, int *ts_count, double t_val) 
{   
    t_val = t_val*(1e12);
    t_val = round(t_val);
    t_val = t_val*(1e-12);
    
    int count = 0;
    
    for(int i=0; i<*ts_count; i++)
    {
        if(ts_arr[i]==t_val)
        {   count = 1;
            break;
        }
    }
    
    if(count == 0)
    {
        ts_arr[*ts_count] = t_val;
        *ts_count = *ts_count + 1;
    }
}

void fillIntersecArr(v3 source, v3 dir, double ts_arr[], int ts_count, v3 intersecArr[]) 
{   
    for(int t_ind=0; t_ind<ts_count; t_ind++)
    {
        intersecArr[t_ind].m_x = source.m_x + ts_arr[t_ind]*dir.m_x;
        intersecArr[t_ind].m_y = source.m_y + ts_arr[t_ind]*dir.m_y;
        intersecArr[t_ind].m_z = source.m_z + ts_arr[t_ind]*dir.m_z;
    }
}

double rayTriangleIntersection(v3 p0, v3 p1, v3 p2, v3 source, v3 dir, bool *val) 
{   
    double epsilon = 1e-8;
    
    v3 p0p1;
    p0p1.m_x = p1.m_x - p0.m_x;
    p0p1.m_y = p1.m_y - p0.m_y;
    p0p1.m_z = p1.m_z - p0.m_z;
    
    v3 p0p2;
    p0p2.m_x = p2.m_x - p0.m_x;
    p0p2.m_y = p2.m_y - p0.m_y;
    p0p2.m_z = p2.m_z - p0.m_z;
    
    v3 pvec;
    crossProduct(dir, p0p2, &pvec);
    
    double det = 0;
    det = dotProduct(p0p1, pvec);
    
    if(det<epsilon && det>-epsilon)
    {
        *val = false;
        return 0;
    }
    
    double invDet = 1/det;
    
    v3 tvec; 
    tvec.m_x = source.m_x - p0.m_x;
    tvec.m_y = source.m_y - p0.m_y;
    tvec.m_z = source.m_z - p0.m_z;
    
    double u = dotProduct(tvec, pvec);
    u = u*invDet;
    
    if(u<0 || u>1)
    {
        *val = false;
        return 0;
    }
    
    v3 qvec;
    crossProduct(tvec, p0p1, &qvec);
    
    double v = dotProduct(dir, qvec);
    v = v*invDet;
    
    if(v<0 || u+v>1)
    {
        *val = false;
        return 0;
    }
    
    double t = dotProduct(p0p2, qvec);
    t = t*invDet;
    
    *val = true;
    return t;
}

double getLengthInMaterial(v3 intersectionsArray[], int N_intersec) 
{   
    double length = 0;
    for(int i=0; i<N_intersec; i=i+2)
    {
        length = length + sqrt(pow((intersectionsArray[i].m_x-intersectionsArray[i+1].m_x),2) + \
                            pow((intersectionsArray[i].m_y-intersectionsArray[i+1].m_y),2) + \
                            pow((intersectionsArray[i].m_z-intersectionsArray[i+1].m_z),2));
    }
    return length;
}

void getTargets(v3 targets[], double detec_dep, int pixels, double detec_size)
{
    double d = detec_size/pixels;
    for(int i=0; i<pixels; i++)
    {
        for(int j=0; j<pixels; j++)
        {
            targets[i*pixels+j].m_x = -(detec_size/2) + d/2 + j*d;
            targets[i*pixels+j].m_y = (detec_size/2) - d/2 - i*d;
            targets[i*pixels+j].m_z = -detec_dep;
        }
    }
}

void getDistanceArray_MT(int i, int j, tri *meshTri, int *meshSize, v3 *source, v3 *targets, double *distArray, const int *pixels)
{
    // int i = blockIdx.x*blockDim.x + threadIdx.x;
    // int j = blockIdx.y*blockDim.y + threadIdx.y;

    if (i < *pixels && j < *pixels)
    {
        int index = i*(*pixels)+j;
        const int arrsize = 10;
        v3 intersecArray[arrsize];
        double ts[arrsize];
        int N_intersec = 0;
        v3 dir;
        
        dir.m_x = targets[index].m_x - source->m_x;
        dir.m_y = targets[index].m_y - source->m_y;
        dir.m_z = targets[index].m_z - source->m_z;
        
        bool tOrF = false;
        double intersection;
        for(int i=0; i<*meshSize; i++)
        {
            intersection  = rayTriangleIntersection(meshTri[i].m_p1, meshTri[i].m_p2, meshTri[i].m_p3, *source, dir, &tOrF);
            if(tOrF==true && N_intersec<arrsize)
            {
                addValue(ts, &N_intersec, intersection);
            }
        }
        
        for(int a=0; a<N_intersec; a++)
        {
            for(int b=a+1; b<N_intersec; b++)
            {
                if(ts[a]>ts[b])
                {
                    double temp = ts[a];
                    ts[a] = ts[b];
                    ts[b] = temp;
                }
            }
        }
        
        fillIntersecArr(*source, dir, ts, N_intersec, intersecArray);
        distArray[index] = getLengthInMaterial(intersecArray, N_intersec);
    }
}

void stlReadIntoVector(vector<tri> *vec, string filename)
{   /* 
    To read the STL file with name "filename" in a vector.
    */
    tri tricoord;
    v3 tempV3[3];
    double temp[3];
    string outerloop;
    string vertex;
    
    ifstream stlfile(filename);
    
    if ( !stlfile )
    {
        cout << "\n";
        cout << "STLA_READ - Fatal error!\n";
        cout << "  Could not open the file \"" << filename << "\".\n";
    }
    
    while(getline(stlfile, outerloop)){
        
        if (outerloop == "  outer loop")
        { 
            for(int i =0; i<3;i++)
            {
                stlfile >> vertex >> temp[0] >> temp[1] >> temp[2];
                if (vertex == "vertex")
                {
                    // setValue(&tempV3[i],temp[0],temp[1],temp[2]);
                    tempV3[i].setValue(temp[0],temp[1],temp[2]);
                }
            }
            tricoord.setValue(tempV3[0],tempV3[1],tempV3[2]);
            vec->push_back(tricoord);
        }
    }
}

void saveToCSV(string name, double * arr, int arrsize)
{   /*
    Stores the 2D array (arr) of size arrsize * arrsize as a CSV file. 
    */
    ofstream image (name);
    if (image.is_open())
    {
        for (int i=0; i<arrsize; i++)
        {
            for (int j=0; j<arrsize; j++)
            {
                image << arr[i*(arrsize)+j] << ",";
            }
            image << "\n";
        }
        image.close();
    }
    else cout << "Unable to open file";
}

// Function to take number of OpenMP threads and size N as user input from command line
void get_input(int argc, char *argv[], int *num_threads)
{
    if (argc != 2)
    {
        printf("Usage: ./main <num_threads>\n");
        exit(1);
    }
    *num_threads = atoi(argv[1]);
    if (*num_threads <= 0)
    {
        printf("num_threads must be a positive integer\n");
        exit(1);
    }
}