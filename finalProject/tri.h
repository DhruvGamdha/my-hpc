#ifndef _TRI_H_
#define _TRI_H_
#include <stdio.h>
#include <stdlib.h>
#include "v3.h"

class tri
{
    public:

    void setValue(v3 p1, v3 p2, v3 p3);
    void printTri();

    v3 m_p1, m_p2, m_p3;
};

void tri::setValue(v3 po1, v3 po2, v3 po3)
{
    this->m_p1 = po1;
    this->m_p2 = po2;
    this->m_p3 = po3;
}

void tri::printTri()
{
    m_p1.printV3();
    m_p2.printV3();
    m_p3.printV3();
}

#endif