#ifndef _UTILS_H_
#define _UTILS_H_
// #include "v3.h"
// #include "tri.h"
// // Inlcude vector header file
// #include <vector>
// // Include math header file
// #include <math.h>

void crossProduct(v3& a, v3& b, v3 *ans);
double dotProduct(v3& a, v3& b);
void addValue(double *ts_arr, int *ts_count, double t_val);
void fillIntersecArr(v3 source, v3 dir, double ts_arr[], int ts_count, v3 intersecArr[]);
double rayTriangleIntersection(v3 p0, v3 p1, v3 p2, v3 source, v3 dir, bool *val);
double getLengthInMaterial(v3 intersectionsArray[], int N_intersec);
void getTargets(v3 targets[], double detec_dep, int pixels, double detec_size);
void getDistanceArray_MT(int i, int j, tri *meshTri, int *meshSize, v3 *source, v3 *targets, double *distArray, const int *pixels);
void stlReadIntoVector(vector<tri> *vec, string filename);
void saveToCSV(string name, double * arr, int arrsize);
void get_input(int argc, char *argv[], int *num_threads);

#endif