#ifndef _V3_H_
#define _V3_H_
// #include <stdio.h>
// #include <stdlib.h>
#include <iostream>
using namespace std;
class v3
{
    public:

    void setValue(double x, double y, double z);
    void printV3();

    double m_x, m_y, m_z;

};

void v3::setValue(double x, double y, double z)
{
    m_x = x;
    m_y = y;
    m_z = z;
}

void v3::printV3()
{
    cout << m_x << "  "<< m_y << "  "<< m_z << endl;
}
#endif