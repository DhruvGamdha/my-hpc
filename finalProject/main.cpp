#include "v3.h"
#include "tri.h"
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;
// Add OpenMP header file
#ifdef _OPENMP
    #include <omp.h>
#endif
#include <sys/time.h>
#include "util.h"

int main(int argc, char *argv[])
{   
    int num_threads;
    void get_input(int argc, char *argv[], int *num_threads);
    get_input(argc, argv, &num_threads);
    vector<tri> stlMesh;    // using vector to read the data from the stl file. 
    string fname = "/stl_files/Rear_grain_R2_centered.stl"; // STL filename with location.
    struct timeval begin, end;          // Variables to time the computation. 
    
    gettimeofday(&begin, NULL);
    void stlReadIntoVector(vector<tri> *vec, string filename);
    stlReadIntoVector(&stlMesh,fname);  // reading of the stl file into vector.
    
    gettimeofday(&end, NULL);
    double readSTLTime = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< " Reading STL file into vector Time : " << readSTLTime << endl;
    
    int totTriangles = stlMesh.size();  // storing the total number of triangles in the mesh.
    cout<< "totTriangles:   "<< totTriangles << endl;
    
    gettimeofday(&begin, NULL);
    tri* meshTriangles = new tri[totTriangles];     // creating array to store the mesh data.
    
    // Changing the datatype of mesh from vector to array for the further use. 
    for(int i=0; i<totTriangles; i++)
    {
        meshTriangles[i].m_p1 = stlMesh[i].m_p1;
        meshTriangles[i].m_p2 = stlMesh[i].m_p2;
        meshTriangles[i].m_p3 = stlMesh[i].m_p3;
    }
    
    gettimeofday(&end, NULL);
    double vecToArrcopyTime = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< "Vector to Array mesh copying Time : " << vecToArrcopyTime << endl;
    
    // setting virtual experimental setup parameters. 
    double detectorDepth = 200;     // negative of the z coordinate of the detector plane, detector plane is perpendicular to z-axis. 
    const int pixels = 2048;        // Image resolution. 
    double detectorSize = 500;      // length and width of the detector. The detector is a square panel. 
    
    v3 source;                      // source coordinates.
    source.setValue(0,0,500);       
    
    double* distanceArr= new double[pixels*pixels];     // array to store the distance values of the ray inside component.
    
    v3* targets = new v3[pixels*pixels];        // array to store the target coordinates for the rays.
    void getTargets(v3 targets[], double detec_dep, int pixels, double detec_size);
    getTargets(targets,detectorDepth, pixels, detectorSize);
    
    gettimeofday(&begin, NULL);
    void getDistanceArray_MT(int i, int j, tri *meshTri, int *meshSize, v3 *source, v3 *targets, double *distArray, const int *pixels);
    // Executing the kernel.
    #pragma omp parallel for num_threads(num_threads)
    for (int i = 0; i < pixels; i++)
        for (int j = 0; j < pixels; j++)
            getDistanceArray_MT(i, j, meshTriangles, &totTriangles, &source, targets, distanceArr, &pixels);
    
    gettimeofday(&end, NULL);
    double GPUtime_P1 = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< "Pattern 1 GPU Time : " << GPUtime_P1 << endl;
    
    // Storing the distance values in depth.csv file.
    string csvname = "depth.csv";
    void saveToCSV(string name, double * arr, int arrsize);
    saveToCSV(csvname, distanceArr, pixels);
    
    // freeing the memory.
    delete[] distanceArr;
    delete[] targets;
    
    return 0;
}
