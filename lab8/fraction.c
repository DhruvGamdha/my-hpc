#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct fraction fraction;
typedef struct properfraction properfraction;
struct fraction
{
   int numer; int denom;
};

struct properfraction
{
   int whole;
   int numer;
   int denom;
};

int main()
{
   fraction a,b,sum, diff, prod, quot;
   properfraction propSum, propDiff, propProd, propQuot;

   printf("Input numerator of a:");
   scanf("%d", &a.numer);

   printf("Input denominator of a:");
   scanf("%d", &a.denom);

   printf("Input numerator of b:");
   scanf("%d", &b.numer);

   printf("Input denominator of b:");
   scanf("%d", &b.denom);

   void fraction_add(const fraction* a,
                     const fraction* b,
                     fraction* sum);

   void fraction_subtract( const fraction* a, 
                           const fraction* b,
                           fraction* diff);
   
   void fraction_mult(  const fraction* a, 
                        const fraction* b,
                        fraction* prod);

   void fraction_divide(const fraction* a, 
                        const fraction* b,
                        fraction* quot);

   void make_proper(const fraction* in,
                  properfraction* out);
   
   void printPropFraction(properfraction val);

   fraction_add(&a,&b,&sum);
   fraction_subtract(&a, &b, &diff);
   fraction_mult(&a, &b, &prod);
   fraction_divide(&a, &b, &quot);

   make_proper(&sum, &propSum);
   make_proper(&diff, &propDiff);
   make_proper(&prod, &propProd);
   make_proper(&quot, &propQuot);

   printf("\n %i/%i  +  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propSum);

   printf("\n %i/%i  -  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propDiff);

   printf("\n %i/%i  x  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propProd);

   printf("\n %i/%i  /  %i/%i  =  ",
          a.numer,a.denom,b.numer,b.denom);

   printPropFraction(propQuot);

   return 0;

}

void printPropFraction(properfraction val)
{
   if (abs(val.whole)>0)
     { printf("%i(%i/%i)",val.whole,val.numer,val.denom); }
   else
     { printf("%i/%i",val.numer,val.denom); }
   printf("\n");
}

void make_proper(const fraction* in,
                  properfraction* out)
{
   fraction temp;
   
   // Shifting the negative value in the denominator to the numerator.
   if (in->denom < 0)
   {
      temp.numer = -1 * in->numer;
      temp.denom = -1 * in->denom;
   }
   else
   {
      temp.numer = in->numer;
      temp.denom = in->denom;
   }

   // Case with zero whole part
   if (abs(temp.numer) < abs(temp.denom))
   {
      out->whole = 0;
      out->numer = temp.numer;
      out->denom = temp.denom;
   }
   else
   {
      int count = 0;
      int val = 1;
      while(1)
      {
         if (abs(val*temp.denom) < abs(temp.numer))
         {
            val = val * temp.denom;
            count++;
         }
         else
            break;
      }

      if (temp.numer < 0)
      {
         out->whole = -1*count;
         out->denom = temp.denom;
         out->numer = -1*temp.numer - val;
      }
      else
      {
         out->whole = count;
         out->denom = temp.denom;
         out->numer = temp.numer - val;
      }
   }

}

void fraction_add(const fraction* a,
                  const fraction* b,
                  fraction* sum)
{
   sum->denom = a->denom * b->denom;
   sum->numer = a->numer * b->denom + b->numer * a->denom;

   void fraction_reduce(fraction* sum);
   fraction_reduce(sum);
}

void fraction_reduce(fraction* sum)
{
   void get_prime_factors(int n,
                          int prime_list[],
                          int* num_primes);

   if ( (sum->numer < 0) && (sum->denom < 0) )
   { sum->numer = abs(sum->numer);
     sum->denom = abs(sum->denom); }

   int prime1[100]; int num_prime_1;
   int msign1 = 1; if (sum->numer < 0) { msign1 = -1; }
   sum->numer = abs(sum->numer);
   get_prime_factors(sum->numer,prime1,&num_prime_1);

   int prime2[100]; int num_prime_2;
   int msign2 = 1; if (sum->denom < 0) { msign2 = -1; }
   sum->denom = abs(sum->denom);
   get_prime_factors(sum->denom,prime2,&num_prime_2);

   int  i = 0; int  j = 0;
   int z1 = prime1[i]; int z2 = prime2[j];

   while(i<num_prime_1 && j<num_prime_2)
   {
      if (z1==z2)
      {
         sum->numer = sum->numer/z1;
         sum->denom = sum->denom/z2;

         i  = i+1;
         j  = j+1;
         z1 = prime1[i];
         z2 = prime2[j];
      }
      else
      {
         if (z1>z2)
         {
            j = j+1;
            z2 = prime2[j];
         }
         else
         {
            i = i+1;
            z1 = prime1[i];
         }
      }
   }

   sum->numer = sum->numer*msign1;
   sum->denom = sum->denom*msign2;
}

void get_prime_factors(int n,
                       int prime_list[],
                       int* num_primes)
{
   *num_primes = 0;

   while (n%2==0)
   {
      prime_list[*num_primes] = 2;
      *num_primes = *num_primes + 1;
      n = n/2;
   }

   for (int i=3; i<=sqrt(n); i=i+2)
   {
      while (n%i==0)
      {
         prime_list[*num_primes] = i;
         *num_primes = *num_primes + 1;
         n = n/i;
      }
   }

   if (n>2)
   {
      prime_list[*num_primes] = n;
      *num_primes = *num_primes + 1;
   }
}

void fraction_subtract( const fraction* a, 
                        const fraction* b,
                        fraction* diff)
{
   diff->denom = a->denom * b->denom;
   diff->numer = a->numer * b->denom - b->numer * a->denom;

   void fraction_reduce(fraction* diff);
   fraction_reduce(diff);
}

void fraction_mult(  const fraction* a, 
                     const fraction* b,
                     fraction* prod)
{
   prod->denom = a->denom * b->denom;
   prod->numer = a->numer * b->numer;

   void fraction_reduce(fraction* prod);
   fraction_reduce(prod);
}

void fraction_divide(const fraction* a, 
                     const fraction* b,
                     fraction* quot)
{
   quot->denom = a->denom * b->numer;
   quot->numer = a->numer * b->denom;

   void fraction_reduce(fraction* quot);
   fraction_reduce(quot);
}