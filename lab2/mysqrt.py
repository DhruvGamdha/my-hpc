''' my own square root function '''

from operator import mod


def sqrt_newton(x,s=1.0,printshow=False):

    kmax = 10;

    for k in range(0,kmax):
        if printshow:
            print("Before iteration %s, s = %20.15f" % (k,s));
        s = 0.5*(s + x/s);

    if printshow:
        print("After %s iterations, s = %20.15f" % (k+1,s));

    return s;

def sqrt_secant(x,s0=0.5, s1=2.5, printshow=False):

    kmax = 10;
    s2 = 0;  
    for k in range(0,kmax):
        if printshow:
            print("Before iteration %s, s = %20.15f" % (k,s2));
        s2 = (x + s1*s0)/(s1 + s0);
        s0 = s1;
        s1 = s2;

    if printshow:
        print("After %s iterations, s = %20.15f" % (k+1,s2));

    return s2;


if __name__ == "__main__":
    
    newton = sqrt_newton(15.0,1.0,True)
    print("Newton sqrt of 15:   ", newton)
    print("----")

    secant = sqrt_secant(15.0, 1.0, 2.0, True)
    print("Secant sqrt of 15:   ", secant)
    
    diff = abs(newton - secant)
    print("Difference between both answers  :", diff)
    # s = sqrt_newton(2.0,1.0,True);

    # print(" ")

    # s = sqrt_newton(8.0,7.0,True);