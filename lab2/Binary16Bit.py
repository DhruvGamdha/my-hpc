def bin2int(bin_string):
    a = []
    intVal = 0

    # Reading and storing the binary string in reverse order.
    for i in range(len(bin_string)):
        a.append(int(bin_string[len(bin_string) -1 - i]))

    # print("a    :", a)
    # Computing the value in base 10
    intVal += -(2**(len(bin_string)-1))*a[len(bin_string)-1]
    for i in range(len(bin_string)-1):
        intVal += (2**i)*a[i]

    return intVal


def int2bin(x):
    bin_string = [];

    if x==0:
        for n in range(0,16):
            bin_string.append('0')
    else:
        if x<0:
            tmp = '1'
            xmod = x + 2**15
        else:
            tmp = '0'
            xmod = x
        
        bin_string.append(tmp)

        for n in range(1, 16):
            pw = 2**(15-n)
            z = int(xmod/pw)
            xmod = xmod - z*pw
            tmp = str(z)
            bin_string.append(tmp)

    bin_string = ''.join(bin_string)

    return bin_string
    

# print(bin2int('1111111100000010'))
# print(int2bin(-254))