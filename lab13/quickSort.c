#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "trimatrix.h"

void quickSort(double* arr, const int start, const int end)
{
    // Function declarations 
    int partition(double* arr,const int start,const int end);
    // Sort the list.
   if(start<end)
   {
      int pivot = partition(arr,start,end);
      quickSort(arr,start,pivot-1);
      quickSort(arr,pivot+1,end);
   }
//    ArrangeIndex(head);
}

int partition(double* arr,const int start,const int end)
{
   double pivot = arr[end];
   int i = start; // Place for swapping
   double temp = 0;

   for(int j=start; j< end; j++)
   {
       if(arr[j] <= pivot)
       {
           temp = arr[i];
           arr[i] = arr[j];
           arr[j] = temp;
           i++;
       }
   }

   // Swap arr[i] to arr[end]
   temp = arr[i];
   arr[i] = arr[end];
   arr[end] = temp;

   return i;
}

double ConditionNumber(const trimatrix* T, double* smallest_abs_eig, double* largest_abs_eig)
{
    // Array of double to store eigenvalues
    int N = T->rows;
    assert(T->rows == T->cols);
    double *eigenVals = (double*)malloc(sizeof(double)*N);

    printf("Eigenvalues:\n");
    for (int i=1; i<=N; i++)
        {
        printf("%f\n",tget(T,i,i));
        eigenVals[i-1] = tget(T,i,i);
        }
    
    // Quick sort declaration:
    void quickSort(double* arr, const int start, const int end);
    quickSort(eigenVals, 0, N-1);

    *smallest_abs_eig = fabs(eigenVals[0]);
    *largest_abs_eig = fabs(eigenVals[N-1]);
    double conditionNum = (*largest_abs_eig)/(*smallest_abs_eig);

    return conditionNum;
}