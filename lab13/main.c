#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "matrix.h"
#include "trimatrix.h"

int main()
{
  srand( time(NULL) );
  int N=0;
  printf("    Input N: ");
  scanf("%i",&N);
  assert(N>1);

  matrix A = new_matrix_function_N(N);

  // Clock times
  clock_t time0,time1;

  // Hessenberg reduction to tridiagonal form
  trimatrix T = new_trimatrix(N);
  void Hessenberg(const matrix* A, trimatrix* T);
  
  time0 = clock();
  Hessenberg(&A,&T);
  time1 = clock();
  double Hessenberg_time = ((double)(time1-time0))/((double)(CLOCKS_PER_SEC));
  printf("\nHessenberg_time = %11.5e\n",Hessenberg_time);

  printf("\n");
  printf("Original Matrix:\n");
  print_matrix(&A);
  printf("Reduction to Tridiagonal Form:\n");
  print_trimatrix(&T);

  // QR Algorithm to find eigenvalues of T
  // which are the same as the eigenvalues of A
  void QRA(trimatrix* T);
  
  time0 = clock();
  QRA(&T);
  time1 = clock();
  double QRA_time = ((double)(time1-time0))/((double)(CLOCKS_PER_SEC));
  printf("\nQRA_time = %11.5e\n",QRA_time);

  printf("After QR Algorithm:\n");
  print_trimatrix(&T);

  double ConditionNumber(const trimatrix* , double* , double* );
  double smallest_abs_eig;
  double largest_abs_eig;
  
  time0 = clock();
  double condition_num = ConditionNumber(&T, &smallest_abs_eig, &largest_abs_eig);
  time1 = clock();
  double CN_time = ((double)(time1-time0))/((double)(CLOCKS_PER_SEC));
  printf("\nCondition_Number_time = %11.5e\n",CN_time);

  printf("\nSmallest eigen value  :%f\n",smallest_abs_eig);
  printf("Largest eigen value     :%f\n",largest_abs_eig);
  printf("Condition number        :%f\n",condition_num);

  delete_matrix(&A);
  delete_trimatrix(&T);
}
